﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CM18tool
{
    public partial class HcLower : Form
    {
        public static string language;
        public static string langFile;
        public static IniFile msgFile;
        Label[] lblTest = new Label[20];
        public static string[] testTitle = new string[20];
        public static string lastCommand;
        public static int commandReply;
        public static int testNum;
        //public SerialPort mySerial;
        public RS232 myRs232 = new RS232();
        public IniFile iniStatus = new IniFile("HCTower.status");
        public IniFile iniConfig = new IniFile("CM18HcLowerCfg.ini");
        public IniFile iniError = new IniFile("HCLower.error");
        public string status = "";
        public Arca.SHCLow myHcLow = new Arca.SHCLow();
        public string fileLogName = "HCLower.log";
        public string SrvFileLogFolder = "";

        public HcLower()//CmDLLs.ConnectionParam connectionParam
        {
            InitializeComponent();
            //lblConnection.Text = connectionParam.pRsConf.device + " ";
            //lblConnection.Text += connectionParam.pRsConf.baudrate + " ";
            InitializeAdditionalComponents();
        }

        public void InitializeAdditionalComponents()
        {
            this.Text = "HC Lower Test v" + Application.ProductVersion;
            myRs232.DataSent += new EventHandler(ComDataSent);
            myRs232.DataReceived += new EventHandler(ComDataReceived);
        }
        public void ComDataSent(object sender, EventArgs e)
        {
            lstComScope.Items.Add("SND:" + sender.ToString());
            lstComScope.SetSelected (lstComScope.Items.Count-1,true);
        }

        public void ComDataReceived(object sender, EventArgs e)
        {
            lstComScope.Items.Add("RCV:" + sender.ToString());
            lstComScope.SetSelected(lstComScope.Items.Count-1, true);
        }

        private void InitializeMyTower()
        {
            Array.Resize(ref myHcLow.tower, 4);
            Array.Resize(ref myHcLow.tower[0].drum, 2);
            Array.Resize(ref myHcLow.tower[1].drum, 2);
            Array.Resize(ref myHcLow.tower[2].drum, 2);
            Array.Resize(ref myHcLow.tower[3].drum, 2);

            myHcLow.tower[0].idTower = 0;
            myHcLow.tower[0].drum[0].address = "4";
            myHcLow.tower[0].drum[0].name = "A";
            myHcLow.tower[0].drum[0].status = "";
            myHcLow.tower[0].drum[1].address = "5";
            myHcLow.tower[0].drum[1].name = "B";
            myHcLow.tower[0].drum[1].status = "";

            myHcLow.tower[1].idTower = 1;
            myHcLow.tower[1].drum[0].address = "6";
            myHcLow.tower[1].drum[0].name = "C";
            myHcLow.tower[1].drum[0].status = "";
            myHcLow.tower[1].drum[1].address = "7";
            myHcLow.tower[1].drum[1].name = "D";
            myHcLow.tower[1].drum[1].status = "";

            myHcLow.tower[2].idTower = 2;
            myHcLow.tower[2].drum[0].address = "8";
            myHcLow.tower[2].drum[0].name = "E";
            myHcLow.tower[2].drum[0].status = "";
            myHcLow.tower[2].drum[1].address = "9";
            myHcLow.tower[2].drum[1].name = "F";
            myHcLow.tower[2].drum[1].status = "";

            myHcLow.tower[3].idTower = 3;
            myHcLow.tower[3].drum[0].address = "A";
            myHcLow.tower[3].drum[0].name = "G";
            myHcLow.tower[3].drum[0].status = "";
            myHcLow.tower[3].drum[1].address = "B";
            myHcLow.tower[3].drum[1].name = "H";
            myHcLow.tower[3].drum[1].status = "";

            for (int i = 0; i <= 3; i++)
            {
                myHcLow.tower[i].phTr1.idPar = "50";
                myHcLow.tower[i].phTr1.name = "phTr1";
            }

            for (int i = 0; i < 4; i++)
            {
                for (int x = 0; x < 2; x++)
                {
                    myHcLow.tower[i].drum[x].phIn.idPar = "30";
                    myHcLow.tower[i].drum[x].phIn.name = "phIn";
                    myHcLow.tower[i].drum[x].phEmpty.idPar = "31";
                    myHcLow.tower[i].drum[x].phEmpty.name = "phEmpty";
                    myHcLow.tower[i].drum[x].phFull.idPar = "32";
                    myHcLow.tower[i].drum[x].phFull.name = "phFull";
                    myHcLow.tower[i].drum[x].phFill.idPar = "33";
                    myHcLow.tower[i].drum[x].phFill.name = "phFill";
                }
            }
        }

        private void HcLower_Load(object sender, EventArgs e)
        {
            language = iniConfig.GetValue("option", "language");
            if (language == "")
            {
                MessageBox.Show("Wrong Language file setting");
            }
            langFile = iniConfig.GetValue("langFile", language);

            if (File.Exists(langFile) == false)
            {
                MessageBox.Show("Language file doesn't exist");
            }

            msgFile = new IniFile(langFile);
            CleanPanel();
            Arca.iniMessage = msgFile;
            IniFile hcLowTest = new IniFile("CM18HcLower.test");
            //string[] value = prova.GetAllValues("connection", "mode");

            string value;
            testNum = 0;
            value = "";
            do
            {
                testNum += 1;
                value = hcLowTest.GetValue("Test" + testNum, "title");
            } while (value != "" & value != "Failed");

            Array.Resize(ref lblTest, testNum);
            Array.Resize(ref testTitle, testNum);

            InitializeMyTower();
            //string[] testt = new string[testNum];
            for (int i = 1; i < testNum; i++)
            {
                lblTest[i] = new Label();
                lblTest[i].ForeColor = Color.Red;
                lblTest[i].Font = new Font("Comics", 9, FontStyle.Bold);
                lblTest[i].AutoSize = true;
                //lblTest[i].Width = 150;
                lblTest[i].Height = 18;
                lblTest[i].Location = new System.Drawing.Point(3, 10 + i * 18);
                lblTest[i].Parent = panelLeft.Panel2;
                lblTest[i].Text = hcLowTest.GetValue("Test" + i, "title").ToUpper();
                lblTest[i].Visible = true;
                testTitle[i] = hcLowTest.GetValue("Test" + i, "tag").ToUpper();
            }

            CmDLLs.ConnPar.pRsConf.device=iniConfig.GetValue("DirectConnect","Port");
            CmDLLs.ConnPar.pRsConf.baudrate=Convert.ToInt32(iniConfig.GetValue("DirectConnect","Baudrate"));
            myRs232.ConfigCom(CmDLLs.ConnPar.pRsConf.device, CmDLLs.ConnPar.pRsConf.baudrate);

            SrvFileLogFolder = iniConfig.GetValue("option", "ServerLogFolder");
            


            btnStart.Enabled = false;
            Arca.WriteMessage(lblMessage, 10);
            this.ActiveControl = txtMain;
        }




        private void CleanPanel()
        {
            lstComScope.Items.Clear();
            LabelReset();
            lblTitle.Text = msgFile.GetValue("messages", "msg1");
            lblMessage.Text = msgFile.GetValue("messages", "msg27");
            txtMain.Text = "";
            txtMain.Enabled = true;
            txtMain.Visible = true;
            txtMain.Focus();
            dgValues.Rows.Clear();
            dgValues.Visible = false;
            this.Text = "HC LOWER TEST";
            btnStart.Visible = true;
            btnStart.Enabled = false;
            
        }

        private void ModeSelection(object sender, EventArgs e)
        {
            singleTestToolStripMenuItem1.Checked = !singleTestToolStripMenuItem1.Checked;
            guidedTestToolStripMenuItem.Checked = !guidedTestToolStripMenuItem.Checked;

            if (guidedTestToolStripMenuItem.Checked==true)
            {
                SingleTestMode(false);
            }
            else
            {
                SingleTestMode(true);
                txtMain.Text = "";
                txtMain.Focus();
            }
        }
        
        private void SingleTestMode(bool activate)
        {
            switch (activate)
            {
                case true:
                    Arca.WriteMessage(lblTitle, 5);
                    Arca.WriteMessage(lblMessage, 11);
                    singleTestToolStripMenuItem.Enabled = true;
                    btnStart.Visible = false;
                    txtMain.Visible = false;
                    break;
                case false:
                    Arca.WriteMessage(lblTitle, 1);
                    Arca.WriteMessage(lblMessage, 10);
                    singleTestToolStripMenuItem.Enabled = false;
                    txtMain.Visible = true;
                    btnStart.Visible = true;
                    txtMain.Focus();
                    break;
            }
            dgValues.Visible = false;
        }

        private void txtMain_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                txtMain.Enabled = false;
                if (txtMain.Text.Length==12)
                {
                    Arca.WriteMessage(lblMessage, 12);
                    if (Arca.WaitingKey() == "ENTER")
                    {
                        btnStart.Enabled = true;
                        txtMain.Visible = false;
                        this.Text = "LOWER SERIAL NUMBER: " + txtMain.Text;
                        Arca.WriteMessage(lblMessage, 13); //Premere il pulsante per avviare il collaudo guidato
                    }
                    else
                    {
                        Arca.WriteMessage(lblMessage, 14); //Inserire la matricola o passa alla modalità singolo test
                        txtMain.Enabled = true;
                        txtMain.Text = "";
                        txtMain.Focus();
                    }
                }
                else
                {
                    Arca.WriteMessage(lblMessage, 500, Arca.errorColor); //La matricola deve essere composta da 12 caratteri. Premere un tasto per ripetere l'inserimento.
                    Arca.WaitingKey();
                    Arca.WriteMessage(lblMessage, 14); //Inserire la matricola o passa alla modalità singolo test
                    txtMain.Enabled = true;
                    txtMain.Text = "";
                    txtMain.Focus();
                }
            }
        }

        

        private void HcLower_KeyUp(object sender, KeyEventArgs e)
        {
            Arca.keyCodePressed = e.KeyValue;
        }

        string TowerPresence(bool singleTest = false)
        {
            string result = "PASS";
            string[] status = new string[4];
            Arca.WriteMessage(lblTitle, 2); //PRESENZA TORRI
            Arca.WriteMessage(lblMessage, 19); //Test presenza torri in corso.
            myRs232.OpenCom();
            myRs232.SendCommand("E12F", 4, 300);
            if (myRs232.commandAnswer=="NO ANSWER")
            {
                result = "KO";
                Arca.WriteMessage(lblMessage, 502, Arca.errorColor);
                goto endCommand;
            }
            myHcLow.tower[0].status = myRs232.commandAnswer.Substring(0, 2);
            myHcLow.tower[1].status = myRs232.commandAnswer.Substring(2, 2);
            myHcLow.tower[2].status = myRs232.commandAnswer.Substring(4, 2);
            myHcLow.tower[3].status = myRs232.commandAnswer.Substring(6, 2);
            myRs232.CloseCom();

            for (int i = 0; i < 4; i++)
            {
                if (myHcLow.tower[i].status != "00")
                { status[i] = "PASS"; }
                else { status[i]="KO";result = "KO"; }
            }

            dgValues.Rows.Clear();
            dgValues.Columns[0].HeaderText = "MODULO";
            dgValues.Columns[1].HeaderText = "STATO";
            dgValues.Columns[2].HeaderText = "DESCRIZIONE";
            dgValues.Columns[3].HeaderText = "ESITO";
            //dgValues.Rows.Add("Module", "State", "Description","Esito");
            dgValues.Rows.Add("0", myHcLow.tower[0].status, iniStatus.GetValue("TowerStatus", myHcLow.tower[0].status), status[0]);
            if (status[0] == "KO") { dgValues.Rows[dgValues.RowCount - 1].DefaultCellStyle.BackColor = Color.Red; }
            dgValues.Rows.Add("1", myHcLow.tower[1].status, iniStatus.GetValue("TowerStatus", myHcLow.tower[1].status), status[1]);
            if (status[1] == "KO") { dgValues.Rows[dgValues.RowCount - 1].DefaultCellStyle.BackColor = Color.Red; }
            dgValues.Rows.Add("2", myHcLow.tower[2].status, iniStatus.GetValue("TowerStatus", myHcLow.tower[2].status), status[2]);
            if (status[2] == "KO") { dgValues.Rows[dgValues.RowCount - 1].DefaultCellStyle.BackColor = Color.Red; }
            dgValues.Rows.Add("3", myHcLow.tower[3].status, iniStatus.GetValue("TowerStatus", myHcLow.tower[3].status), status[3]);
            if (status[3] == "KO") { dgValues.Rows[dgValues.RowCount - 1].DefaultCellStyle.BackColor = Color.Red; }
            dgValues.AutoResizeColumns();
            dgValues.Visible = true;
endCommand:
            if (singleTest==false)
            {
                if (result == "PASS") { Arca.WaitingTime(1000); }
                string log = ",410" + myHcLow.tower[0].status + ",411" + myHcLow.tower[1].status + ",412" 
                    + myHcLow.tower[2].status + ",413" + myHcLow.tower[3].status;
                Arca.LogWrite(fileLogName, log);
            }
            
            return result;
        }

        string InputPhoto(bool singleTest = false)
        {
            string result="PASS";
            Arca.WriteMessage(lblTitle, 3); //PRISMA INGRESSO
            Arca.WriteMessage(lblMessage, 15); //Calibrazione foto in corso
            myHcLow.phInA.adjMin = Convert.ToInt16(iniConfig.GetValue("PhotoRange", "phInAMin"));
            myHcLow.phInA.adjMax = Convert.ToInt16(iniConfig.GetValue("PhotoRange", "phInAMax"));
            myHcLow.phInB.adjMin = Convert.ToInt16(iniConfig.GetValue("PhotoRange", "phInBMin"));
            myHcLow.phInB.adjMax = Convert.ToInt16(iniConfig.GetValue("PhotoRange", "phInBMax"));

            myRs232.OpenCom();
            myRs232.SendCommand("D171", 1, 300);
            //result = myRs232.commandAnswer;

            myRs232.SendCommand("E128", 4, 100);
            myHcLow.phInA.name = "phInA";
            myHcLow.phInA.value = Convert.ToInt16(Arca.Hex2Ascii(myRs232.commandAnswer));

            myRs232.SendCommand("E129", 4, 100);
            myHcLow.phInB.name = "phInB";
            myHcLow.phInB.value = Convert.ToInt16(Arca.Hex2Ascii(myRs232.commandAnswer));

            myRs232.CloseCom();

            dgValues.Rows.Clear();
            dgValues.Columns[0].HeaderText = "FOTO";
            dgValues.Columns[1].HeaderText = "VALORE";
            dgValues.Columns[2].HeaderText = "RANGE";
            dgValues.Columns[3].HeaderText = "ESITO";
            if (PhotoVerify(ref myHcLow.phInA, myHcLow.phInA.value) == "KO") { result = "KO"; }

            if (PhotoVerify(ref myHcLow.phInB, myHcLow.phInB.value) == "KO") { result = "KO"; }

            dgValues.AutoResizeColumns();
            dgValues.Visible = true;

            if (singleTest == false)
            {
                if (result == "PASS") { Arca.WaitingTime(1000); dgValues.Visible = false; }
                string log = ",2D0" + myHcLow.phInA.value + ",2D1" + myHcLow.phInA.value;
                Arca.LogWrite(fileLogName, log);
            }
            
            return result;
        }

        private void singleTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Arca.WriteMessage(lblTitle, 5);
            Arca.WriteMessage(lblMessage, 11);
            dgValues.Visible = false;
            dgValues.Rows.Clear();
        }

        string PhotoVerify(ref Arca.SPhoto photo, int value)
        {
            string range = "";
            string result = "PASS";
            photo.adjMax = Convert.ToInt16(iniConfig.GetValue("PhotoRange", photo.name + "max"));
            photo.adjMin = Convert.ToInt16(iniConfig.GetValue("PhotoRange", photo.name + "min"));
            range = photo.adjMin + " - " + photo.adjMax;
            photo.value = value;
            if ((photo.value >= photo.adjMin) && (photo.value <= photo.adjMax))
            {
                photo.adjResult = "PASS";
            }
            else
            {
                result = "KO";
                photo.adjResult = "KO";
            }
            dgValues.Rows.Add(photo.name, photo.value, range, photo.adjResult);
            if (photo.adjResult == "KO")
            {
                //dgValues.Rows[dgValues.RowCount - 1].Cells[1].Style.BackColor = Color.Red;
                dgValues.Rows[dgValues.RowCount - 1].DefaultCellStyle.BackColor = Color.Red;
                dgValues.CurrentCell = dgValues.Rows[dgValues.RowCount - 1].Cells[0];
                dgValues.Rows[dgValues.RowCount - 1].Selected = true;
            }
            return result;
        }

        string VerticalMotor(bool singleTest = false)
        {
            start:
            Arca.WriteMessage(lblTitle, 4); //MOTORE TRASPORTO VERTICALE
            Arca.WriteMessage(lblMessage, 16); //Verificare il corretto funzionamento del motore del trasporto verticale e della ventola
            string result = "PASS";

            myRs232.OpenCom();
            for (int i = 1; i <= 5; i++)
            {
                myRs232.SendCommand("D16500", 1, 3000);
                //result = myRs232.commandAnswer;
                Arca.WaitingTime(3000);
            }
            myRs232.CloseCom();

            if (singleTest == false)
            {
                if (result == "PASS") { Arca.WaitingTime(1000); }
                string log = ",303" + result;
                Arca.LogWrite(fileLogName, log);
            }

            Arca.WriteMessage(lblMessage, 17); //Se il motore del trasporto verticale e della ventola funzionano correttamente premere un tasto per continuare, 'R' per ripetere, ESC per uscire
            switch (Arca.WaitingKey())
            {
                case "R":
                    goto start;
                case "ESC":
                    result = "KO";
                    break;
            }
            if (result == "KO")
            {

            }
            return result;
        }

        private void lblConnection_Click(object sender, EventArgs e)
        {
            MessageBox.Show("i parametri si possono cambiare dal file ini");
        }

        private void presenzaTorriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TowerPresence(true);
        }

        private void lstComScope_SizeChanged(object sender, EventArgs e)
        {

        }

        private void prismiIngressoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InputPhoto(true);
        }

        private void motoreTraspVertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VerticalMotor(true);
            
        }

        void LabelReset()
        {
            foreach (Control myLabel in panelLeft.Panel2.Controls)
            {
                if (myLabel is Label)
                {
                    if (myLabel.Text != "Test:")
                    {
                        myLabel.ForeColor = Arca.errorColor;
                    }
                }
            }
        }

        string DoorSwitch()
        {
            string result = "PASS";
            myRs232.OpenCom();
            myRs232.SendCommand("E133", 2, 1000);
            
            myRs232.CloseCom();
            return result;
        }

        string AlarmSwitch()
        {
            string result = "PASS";
            myRs232.OpenCom();
            myRs232.SendCommand("E12A", 2, 1000);

            myRs232.CloseCom();
            return result;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Visible = false;
            string logsLine = "";
            string FailedTest = "";
            string testResult = "PASS";
            logsLine += "004" + txtMain.Text; //Serial Number
            logsLine += ",005" + Application.ProductVersion; //SW Version
            logsLine += ",006" + System.Environment.MachineName; //Computer Name
            logsLine += ",300" + DateTime.Now.Date.ToShortDateString(); //Data Start
            logsLine += ",301" + DateTime.Now.ToLongTimeString(); //Time Start
            Arca.LogWrite(fileLogName, "", true);
            Arca.LogWrite(fileLogName, logsLine);

        start:
            LabelReset();

            for (int i = 1; i < testNum; i++)
            {
                lblTest[i].ForeColor = Color.LightYellow;
                Arca.WaitingTime(200);
                switch (testTitle[i])
                {
                    case "TOWERPRESENCE":
                        if (TowerPresence() == "PASS") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Tower Presence /";
                        }
                        break;
                    case "INPUTPRISM":
                        if (InputPhoto() == "PASS") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Photo Calibration /";
                        }
                        break;
                    case "VERTICALMOTOR":
                        if (VerticalMotor() == "PASS") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Vertical Motor /";
                        }
                        break;
                    case "DOORSWITCH":
                        if (DoorSwitch() == "PASS") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Door Switch /";
                        }
                        break;
                    case "ALARMSWITCH":
                        if (AlarmSwitch() == "PASS") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Alarm Switch /";
                        }
                        break;
                    default:
                        break;
                }
            }

            if (FailedTest.Length==0)
            {
                Arca.WriteMessage(lblTitle, 6);
                Arca.WriteMessage(lblMessage, 18);
            }
            else
            {
                Arca.WriteMessage(lblTitle, 6);
                Arca.WriteMessage(lblMessage, 501);
                testResult = "KO";
            }

            Arca.LogWrite(fileLogName, ",305" + DateTime.Now.Date.ToShortDateString()); //Data END
            Arca.LogWrite(fileLogName, ",306" + DateTime.Now.ToLongTimeString()); //Time END
            Arca.LogWrite(fileLogName, ",302" + testResult); //Test Result
            Arca.WaitingKey();
            if (File.Exists(fileLogName))
            {
                Arca.WriteMessage(lblMessage, 20);
                Arca.SendLog(fileLogName, SrvFileLogFolder);
            }
            CleanPanel();
            
        }

        private void sensoriPortaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DoorSwitch();
        }

        private void sensoriAlarmeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AlarmSwitch();
        }

        private void comandoSingoloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }
    }
}

