﻿namespace CM18tool
{
    partial class HcLower
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HcLower));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelMain = new System.Windows.Forms.SplitContainer();
            this.panelLeft = new System.Windows.Forms.SplitContainer();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.lstComScope = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMain = new System.Windows.Forms.TextBox();
            this.dgValues = new System.Windows.Forms.DataGridView();
            this.col1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnStart = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.connectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singleTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.presenzaTorriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prismiIngressoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.motoreTraspVertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singleTestToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.guidedTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblConnection = new System.Windows.Forms.ToolStripStatusLabel();
            this.sensoriPortaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sensoriAlarmeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelMain)).BeginInit();
            this.panelMain.Panel1.SuspendLayout();
            this.panelMain.Panel2.SuspendLayout();
            this.panelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelLeft)).BeginInit();
            this.panelLeft.Panel1.SuspendLayout();
            this.panelLeft.Panel2.SuspendLayout();
            this.panelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgValues)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMain
            // 
            this.panelMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 24);
            this.panelMain.Name = "panelMain";
            // 
            // panelMain.Panel1
            // 
            this.panelMain.Panel1.Controls.Add(this.panelLeft);
            // 
            // panelMain.Panel2
            // 
            this.panelMain.Panel2.Controls.Add(this.txtMain);
            this.panelMain.Panel2.Controls.Add(this.dgValues);
            this.panelMain.Panel2.Controls.Add(this.btnStart);
            this.panelMain.Panel2.Controls.Add(this.lblMessage);
            this.panelMain.Panel2.Controls.Add(this.lblTitle);
            this.panelMain.Size = new System.Drawing.Size(956, 543);
            this.panelMain.SplitterDistance = 317;
            this.panelMain.TabIndex = 0;
            this.panelMain.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HcLower_KeyUp);
            // 
            // panelLeft
            // 
            this.panelLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(0, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // panelLeft.Panel1
            // 
            this.panelLeft.Panel1.Controls.Add(this.picLogo);
            // 
            // panelLeft.Panel2
            // 
            this.panelLeft.Panel2.Controls.Add(this.lstComScope);
            this.panelLeft.Panel2.Controls.Add(this.label1);
            this.panelLeft.Size = new System.Drawing.Size(317, 543);
            this.panelLeft.SplitterDistance = 88;
            this.panelLeft.TabIndex = 0;
            this.panelLeft.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HcLower_KeyUp);
            // 
            // picLogo
            // 
            this.picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            this.picLogo.Location = new System.Drawing.Point(12, 12);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(52, 59);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 3;
            this.picLogo.TabStop = false;
            // 
            // lstComScope
            // 
            this.lstComScope.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstComScope.FormattingEnabled = true;
            this.lstComScope.Location = new System.Drawing.Point(6, 278);
            this.lstComScope.Name = "lstComScope";
            this.lstComScope.Size = new System.Drawing.Size(306, 134);
            this.lstComScope.TabIndex = 5;
            this.lstComScope.SizeChanged += new System.EventHandler(this.lstComScope_SizeChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Test:";
            // 
            // txtMain
            // 
            this.txtMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtMain.Location = new System.Drawing.Point(7, 198);
            this.txtMain.MaxLength = 12;
            this.txtMain.Name = "txtMain";
            this.txtMain.Size = new System.Drawing.Size(615, 24);
            this.txtMain.TabIndex = 14;
            this.txtMain.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMain_KeyUp);
            // 
            // dgValues
            // 
            this.dgValues.AllowUserToAddRows = false;
            this.dgValues.AllowUserToDeleteRows = false;
            this.dgValues.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgValues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgValues.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col1,
            this.col2,
            this.col3,
            this.col4});
            this.dgValues.Location = new System.Drawing.Point(7, 228);
            this.dgValues.Name = "dgValues";
            this.dgValues.ReadOnly = true;
            this.dgValues.Size = new System.Drawing.Size(614, 221);
            this.dgValues.TabIndex = 13;
            this.dgValues.Visible = false;
            // 
            // col1
            // 
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col1.DefaultCellStyle = dataGridViewCellStyle13;
            this.col1.HeaderText = "Name";
            this.col1.Name = "col1";
            this.col1.ReadOnly = true;
            // 
            // col2
            // 
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col2.DefaultCellStyle = dataGridViewCellStyle14;
            this.col2.HeaderText = "Value";
            this.col2.Name = "col2";
            this.col2.ReadOnly = true;
            // 
            // col3
            // 
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col3.DefaultCellStyle = dataGridViewCellStyle15;
            this.col3.HeaderText = "Range";
            this.col3.Name = "col3";
            this.col3.ReadOnly = true;
            // 
            // col4
            // 
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col4.DefaultCellStyle = dataGridViewCellStyle16;
            this.col4.HeaderText = "Result";
            this.col4.Name = "col4";
            this.col4.ReadOnly = true;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(256, 132);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(122, 60);
            this.btnStart.TabIndex = 12;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.Location = new System.Drawing.Point(3, 47);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(618, 80);
            this.lblMessage.TabIndex = 11;
            this.lblMessage.Text = "lblMessage";
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(3, 12);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(618, 28);
            this.lblTitle.TabIndex = 10;
            this.lblTitle.Text = "lblTitle";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectionToolStripMenuItem,
            this.singleTestToolStripMenuItem,
            this.modeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(956, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // connectionToolStripMenuItem
            // 
            this.connectionToolStripMenuItem.Name = "connectionToolStripMenuItem";
            this.connectionToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.connectionToolStripMenuItem.Text = "Connection";
            // 
            // singleTestToolStripMenuItem
            // 
            this.singleTestToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.presenzaTorriToolStripMenuItem,
            this.prismiIngressoToolStripMenuItem,
            this.motoreTraspVertToolStripMenuItem,
            this.sensoriPortaToolStripMenuItem,
            this.sensoriAlarmeToolStripMenuItem});
            this.singleTestToolStripMenuItem.Enabled = false;
            this.singleTestToolStripMenuItem.Name = "singleTestToolStripMenuItem";
            this.singleTestToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.singleTestToolStripMenuItem.Text = "Single Test";
            this.singleTestToolStripMenuItem.Click += new System.EventHandler(this.singleTestToolStripMenuItem_Click);
            // 
            // presenzaTorriToolStripMenuItem
            // 
            this.presenzaTorriToolStripMenuItem.Name = "presenzaTorriToolStripMenuItem";
            this.presenzaTorriToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.presenzaTorriToolStripMenuItem.Text = "Presenza Torri";
            this.presenzaTorriToolStripMenuItem.Click += new System.EventHandler(this.presenzaTorriToolStripMenuItem_Click);
            // 
            // prismiIngressoToolStripMenuItem
            // 
            this.prismiIngressoToolStripMenuItem.Name = "prismiIngressoToolStripMenuItem";
            this.prismiIngressoToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.prismiIngressoToolStripMenuItem.Text = "Prismi ingresso";
            this.prismiIngressoToolStripMenuItem.Click += new System.EventHandler(this.prismiIngressoToolStripMenuItem_Click);
            // 
            // motoreTraspVertToolStripMenuItem
            // 
            this.motoreTraspVertToolStripMenuItem.Name = "motoreTraspVertToolStripMenuItem";
            this.motoreTraspVertToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.motoreTraspVertToolStripMenuItem.Text = "Motore trasp vert";
            this.motoreTraspVertToolStripMenuItem.Click += new System.EventHandler(this.motoreTraspVertToolStripMenuItem_Click);
            // 
            // modeToolStripMenuItem
            // 
            this.modeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.singleTestToolStripMenuItem1,
            this.guidedTestToolStripMenuItem});
            this.modeToolStripMenuItem.Name = "modeToolStripMenuItem";
            this.modeToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.modeToolStripMenuItem.Text = "Mode";
            // 
            // singleTestToolStripMenuItem1
            // 
            this.singleTestToolStripMenuItem1.Name = "singleTestToolStripMenuItem1";
            this.singleTestToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.singleTestToolStripMenuItem1.Text = "Single Test";
            this.singleTestToolStripMenuItem1.Click += new System.EventHandler(this.ModeSelection);
            // 
            // guidedTestToolStripMenuItem
            // 
            this.guidedTestToolStripMenuItem.Checked = true;
            this.guidedTestToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.guidedTestToolStripMenuItem.Name = "guidedTestToolStripMenuItem";
            this.guidedTestToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.guidedTestToolStripMenuItem.Text = "Guided Test";
            this.guidedTestToolStripMenuItem.Click += new System.EventHandler(this.ModeSelection);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblConnection});
            this.statusStrip1.Location = new System.Drawing.Point(0, 545);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(956, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(129, 17);
            this.toolStripStatusLabel1.Text = "Connection Parameter:";
            // 
            // lblConnection
            // 
            this.lblConnection.Name = "lblConnection";
            this.lblConnection.Size = new System.Drawing.Size(13, 17);
            this.lblConnection.Text = "  ";
            this.lblConnection.Click += new System.EventHandler(this.lblConnection_Click);
            // 
            // sensoriPortaToolStripMenuItem
            // 
            this.sensoriPortaToolStripMenuItem.Name = "sensoriPortaToolStripMenuItem";
            this.sensoriPortaToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.sensoriPortaToolStripMenuItem.Text = "Sensori Porta";
            this.sensoriPortaToolStripMenuItem.Click += new System.EventHandler(this.sensoriPortaToolStripMenuItem_Click);
            // 
            // sensoriAlarmeToolStripMenuItem
            // 
            this.sensoriAlarmeToolStripMenuItem.Name = "sensoriAlarmeToolStripMenuItem";
            this.sensoriAlarmeToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.sensoriAlarmeToolStripMenuItem.Text = "Sensori Alarme";
            this.sensoriAlarmeToolStripMenuItem.Click += new System.EventHandler(this.sensoriAlarmeToolStripMenuItem_Click);
            // 
            // HcLower
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 567);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "HcLower";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HC Lower Test";
            this.Load += new System.EventHandler(this.HcLower_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HcLower_KeyUp);
            this.panelMain.Panel1.ResumeLayout(false);
            this.panelMain.Panel2.ResumeLayout(false);
            this.panelMain.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelMain)).EndInit();
            this.panelMain.ResumeLayout(false);
            this.panelLeft.Panel1.ResumeLayout(false);
            this.panelLeft.Panel2.ResumeLayout(false);
            this.panelLeft.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelLeft)).EndInit();
            this.panelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgValues)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer panelMain;
        private System.Windows.Forms.SplitContainer panelLeft;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem connectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singleTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singleTestToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem guidedTestToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMain;
        public System.Windows.Forms.DataGridView dgValues;
        private System.Windows.Forms.DataGridViewTextBoxColumn col1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col2;
        private System.Windows.Forms.DataGridViewTextBoxColumn col3;
        private System.Windows.Forms.DataGridViewTextBoxColumn col4;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ToolStripMenuItem presenzaTorriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prismiIngressoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem motoreTraspVertToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblConnection;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ListBox lstComScope;
        private System.Windows.Forms.ToolStripMenuItem sensoriPortaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sensoriAlarmeToolStripMenuItem;
    }
}